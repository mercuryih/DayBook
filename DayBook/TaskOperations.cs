﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DayBook
{
    public class TaskOperations
    {
        public void AddNewTask(string date, string data)
        {
            DBOperations dBO = new DBOperations();
            if (CheckTaskCorrect(data) == 0)
                dBO.DBAddNewItem(date, data);
        }

        public int CheckTaskCorrect(string data)
        {
            if (String.IsNullOrEmpty(data))
                return -1;
            if (data.Length > 1000)
                return 1;

            return 0;
        }
    }
}
