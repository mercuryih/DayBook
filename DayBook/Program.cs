﻿using System;
using System.Configuration;

namespace DayBook
{
    class Program
    {
        static void Main(string[] args)
        {
            DBConn.SetConnection(ConfigurationManager.AppSettings["DefaultDBConnStr"]);
        }
    }
}
